import java.util.Scanner;
public class array {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    int anzahl;
    
  
    //Deklaration eines Arrays, mit Custom integer Werte
    //Name des Arrays: "intArray"
    System.out.println("Wieviele Werte soll ihr Array haben?:");
    anzahl = sc.nextInt();
    
    int [] intArray = new int [anzahl];

    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    intArray[2] = 1000;
    intArray[4] = 500;
    
  
    
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 

    double [] doubleArray = {1.1,2.2,3.3,4.4};
 
    
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    System.out.println(doubleArray[1]);
    
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    int anzahl2;
    int wert;
    
    System.out.println("An welchen Index soll der neue Wert?");
    anzahl2 = sc.nextInt();
    
   System.out.println("Geben Sie den neuen Wert f�r index " + anzahl2 + " an:");
   wert = sc.nextInt();
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // 0   0   1000  0   0  
   for(int index = 0; index < intArray.length; index++) {
   	System.out.println("Index " + index + ": " +  intArray[index]);
   }
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion "lenght"
    
   for(int anzahl2 = 0; anzahl2 < doubleArray.length; anzahl2++)
   { doubleArray[anzahl2] = 0;
   	System.out.println(anzahl2 + 1 + ". " + doubleArray[anzahl2]);
   }
    
    //Der intArray soll mit neuen Werten gef�llt werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    int j = 10;
    for(int e = 0; e < 5; e++)
    { intArray[e] = j;
    j+ = 10;
    System.out.println("Ausgabe:" + intArray[1]);
    
    }
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrung