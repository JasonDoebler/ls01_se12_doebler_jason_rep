
public class Fahrkarte {
	private int fahrkartenNummer;
	private double preis;
	private String bezeichnung;
	
	public Fahrkarte(int fahrkartenNummer) {
		super();
		this.fahrkartenNummer = fahrkartenNummer;
		festlegenPreis();
	}
	
	public void festlegenPreis() {
		switch (fahrkartenNummer) {
			case 1:
				this.setBezeichnung("Einzelfahrschein Berlin AB");
				this.setPreis(2.9);
				break;
			case 2:
				this.setBezeichnung("Einzelfahrschein Berlin BC");
				this.setPreis(3.3);
				break;
			case 3:
				this.setBezeichnung("Einzelfahrschein Berlin ABC");
				this.setPreis(3.6);
				break;
			case 4:
				this.setBezeichnung("Kurzstrecke");
				this.setPreis(1.9);
				break;
			case 5:
				this.setBezeichnung("Tageskarte Berlin AB");
				this.setPreis(8.6);
				break;
			case 6:
				this.setBezeichnung("Tageskarte Berlin BC");
				this.setPreis(9.0);
				break;
			case 7:
				this.setBezeichnung("Tageskarte Berlin ABC");
				this.setPreis(9.6);
				break;
			case 8:
				this.setBezeichnung("Kleingruppen-Tageskarte Berlin AB");
				this.setPreis(23.5);
				break;
			case 9:
				this.setBezeichnung("Kleingruppen-Tageskarte Berlin BC");
				this.setPreis(24.3);
				break;
			case 10:
				this.setBezeichnung("Kleingruppen-Tageskarte Berlin ABC");
				this.setPreis(24.9);
				break;
			default:
				System.out.println("Fehleingabe");
				break;
		}
	}
	

	public int getFahrkartenNummer() {
		return fahrkartenNummer;
	}

	public void setFahrkartenNummer(int fahrkartenNummer) {
		this.fahrkartenNummer = fahrkartenNummer;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

}
