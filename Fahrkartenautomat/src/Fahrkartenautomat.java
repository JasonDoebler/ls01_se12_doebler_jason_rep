import java.util.Scanner;

class Fahrkartenautomat {
	public static Fahrkarte[] alleFahrkarten;
	public static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args) {
    	while(true) {
    		bestellvorgang();
    	}
    }
    
    public static void bestellvorgang() {
        
        double zuZahlenderBetrag; 
        double eingezahlterGesamtbetrag;
        double r�ckgabebetrag;

        zuZahlenderBetrag = Fahrkartenautomat.fahrkartenbestellungErfassen(); 
        if(zuZahlenderBetrag == 0) {
        	System.out.println("\n-------------------\n");
        	return;
        }
        eingezahlterGesamtbetrag = Fahrkartenautomat.fahrkartenBezahlen(zuZahlenderBetrag); 
        Fahrkartenautomat.fahrkartenAusgeben(); 
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; 
        r�ckgabebetrag = runden(r�ckgabebetrag, 2);
        Fahrkartenautomat.rueckgeldAusgeben(r�ckgabebetrag);
    }
    
    
    
    public static double fahrkartenbestellungErfassen() {
    	double zuZahlenderBetrag;
    	int fahrkartenNummer;
    	byte anzahlTickets = 0;
    	
    	System.out.println("Welches Ticket m�chten Sie kaufen? ");
    	ausgebenFahrkartenArten();
    	fahrkartenNummer = tastatur.nextInt();
    	
    	if(fahrkartenNummer > 0 && fahrkartenNummer < 11) {
    		System.out.println("Anzahl der Tickets (1-10): ");
            anzahlTickets = tastatur.nextByte();
            if(anzahlTickets < 0 || anzahlTickets >10) {
            	System.out.println("Fehleingabe");
            	Fahrkartenautomat.fahrkartenbestellungErfassen();
            }
            
            alleFahrkarten = new Fahrkarte[anzahlTickets];
            
    		for(int i=0; i<anzahlTickets; i++) {
    			Fahrkarte fahrkarte = new Fahrkarte(fahrkartenNummer);
    			alleFahrkarten[i] = fahrkarte;
    		}
    		
    	} else {
    		System.out.println("Fehleingabe");
    		Fahrkartenautomat.fahrkartenbestellungErfassen();
    	}
    	
        zuZahlenderBetrag = alleFahrkarten[0].getPreis();
        zuZahlenderBetrag = runden(zuZahlenderBetrag, 2);
        zuZahlenderBetrag = zuZahlenderBetrag*anzahlTickets;
        zuZahlenderBetrag = runden(zuZahlenderBetrag, 2);
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneM�nze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
        	double d = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	    d = runden(d, 2);
     	    System.out.printf("Noch zu zahlen: %.2f Euro \n", d);
     	    System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	    eingeworfeneM�nze = tastatur.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        
        for (int i = 0; i < 26; i++) {
        	System.out.print("=");
        	warte(65);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0) {
    		System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO \n", r�ckgabebetrag);
     	    System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) {
            	m�nzeAusgeben(2, "Euro");
 	            r�ckgabebetrag -= 2.0;
 	            r�ckgabebetrag = runden(r�ckgabebetrag, 2);
            }
            while(r�ckgabebetrag >= 1.0) {
            	m�nzeAusgeben(1, "Euro");
 	            r�ckgabebetrag -= 1.0;
 	            r�ckgabebetrag = runden(r�ckgabebetrag, 2);
            }
            while(r�ckgabebetrag >= 0.5) {
            	m�nzeAusgeben(50, "Cent");
 	            r�ckgabebetrag -= 0.5;
 	            r�ckgabebetrag = runden(r�ckgabebetrag, 2);
            }
            while(r�ckgabebetrag >= 0.2) {
            	m�nzeAusgeben(20, "Cent");
  	            r�ckgabebetrag -= 0.2;
  	            r�ckgabebetrag = runden(r�ckgabebetrag, 2);
            }
            while(r�ckgabebetrag >= 0.1) {
            	m�nzeAusgeben(10, "Cent");
 	            r�ckgabebetrag -= 0.1;
 	            r�ckgabebetrag = runden(r�ckgabebetrag, 2);
            }
            while(r�ckgabebetrag >= 0.05) {
            	m�nzeAusgeben(5, "Cent");
  	            r�ckgabebetrag -= 0.05;
  	            r�ckgabebetrag = runden(r�ckgabebetrag, 2);
            }
        }
        System.out.println("\nVergessen Sie den Fahrschein nicht\n"+"vor Fahrtantritt den Schein entwerten zu lassen!\n"+"Wir w�nschen eine gute Fahrt.");
        System.out.println("\n-----------------------------\n");
    }
    
 
    public static double runden(double wert, int nachkommaStellen) {
        double d = Math.pow(10, nachkommaStellen);
        return Math.round(wert * d) / d;
    }
    
    public static void warte(int millis) {
    	try {
    		Thread.sleep(millis);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }
    
    public static void ausgebenFahrkartenArten() {
    	for(int i=1; i<11; i++) {
    		Fahrkarte temp = new Fahrkarte(i);
    		System.out.println(i + ": " + temp.getBezeichnung());
    	}
    }
    
    public static void m�nzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    
}