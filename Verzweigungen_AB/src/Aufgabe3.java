import java.util.Scanner;

public class Aufgabe3 {

	public static void main(String[] args) {
		Scanner Eingabe = new Scanner(System.in);

		System.out.println("Bitte geben Sie eine Zahl ein:");
		int zahl = Eingabe.nextInt();

		if (zahl < 0) {
			System.out.println("Die Zahl ist negativ!");
		} else {
			System.out.println("Die Zahl ist positiv!");
		}
		Eingabe.close();
	}

}
