import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		Scanner Eingabe = new Scanner(System.in);

		System.out.println("Bitte den ersten Wert eingeben:");
		int a = Eingabe.nextInt();
		System.out.println("Bitte den zweiten Wert eingeben:");
		int b = Eingabe.nextInt();
		System.out.println("");

		if (a == b) {
			System.out.print(a);
			System.out.print(b);
		} else {
			System.out.println(a);
			System.out.println(b);
		}

		Eingabe.close();
	}

}
