import java.util.Scanner;

public class Aufgabe5 {

	public static void main(String[] args) {
		Scanner Eingabe = new Scanner(System.in);

		System.out.println("Bitte geben sie eine Zahl");

		int zahl = Eingabe.nextInt();

		if (zahl % 2 == 0) {
			System.out.println(zahl / 2);
		} else {
			System.out.println("Zahl ungerade");
		}

		Eingabe.close();
	}

}
