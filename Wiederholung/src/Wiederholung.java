
public class konsoleTest {

	public static void main(String[] args) {
		
	//Aufgabe 1
		
		String bsp = "Hallo, dies ist ein Beispiel";
		String bsp2 = "Hallo, dies ist ein weiteres Beispiel";
		
	
	System.out.print("Hallo, dies ist ein Beispiel");
	System.out.print("Hallo, dies ist ein weiteres Beispiel\n");
	System.out.println(bsp);
	System.out.println(bsp2);
	
	// print() sorgt in der Ausgabe daf�r, dass der gezeigte Text in einer Zeile ist.
	// println() sorgt in der Ausgabe daf�r, dass die gezeigten Texte , untereinander stehen.
	
	//Aufgabe 2
	
	System.out.println("\n");
	System.out.println("      *");
	System.out.println("     ***");
	System.out.println("    *****");
	System.out.println("   *******");
	System.out.println("  *********");
	System.out.println(" ***********");
	System.out.println("*************");
	System.out.println("     ***");
	System.out.println("     ***");
	
	//Durch die bestimmte plazierung der Symbole, wird ein Pfeil ausgegeben
	
	//Aufgabe 3
	
	System.out.println();
	System.out.printf("%.2f\n" , 22.4234234);
	System.out.printf("%.2f\n" , 111.2222);
	System.out.printf("%.2f\n" , 4.0);
	System.out.printf("%.2f\n" , 1000000.551);
	System.out.printf("%.2f\n" , 97.34);
	
	//System.out.printf("%.2f\n",  ) k�rzt zahlen auf zwei stellen nach den komma
	
	}
	
}
