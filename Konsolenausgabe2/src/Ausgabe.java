public class Ausgabe {
	
	public static void main(String[] args) {

	//Aufgabe1		
		
		String stern = "*";
	
			System.out.printf( "%10s%-8s\n", stern, stern);
			System.out.printf( "%7s%7s\n", stern, stern);
			System.out.printf( "%7s%7s\n", stern, stern);
			System.out.printf( "%10s%-8s\n", stern, stern);
			
			System.out.println();
			System.out.println();
			System.out.println();
			
	//Aufgabe2
			
			System.out.printf("%-5s=%-19s=%4s\n", "0!", "" , "1");
			System.out.printf("%-5s=%-19s=%4s\n", "1!", " 1" , "1");
			System.out.printf("%-5s=%-19s=%4s\n", "2!", " 1 * 2" , "2");
			System.out.printf("%-5s=%-19s=%4s\n", "3!", " 1 * 2 * 3" , "6");
			System.out.printf("%-5s=%-19s=%4s\n", "4!", " 1 * 2 * 3 * 4" , "24");
			System.out.printf("%-5s=%-19s=%4s\n", "5!", " 1 * 2 * 3 * 4 * 5" , "120");
			
			System.out.println();
			System.out.println();
			System.out.println();
			
	//Aufgabe3
			
			System.out.printf("%-12s%-5s%10s\n", "Fahrenheit","|", "Celsius");
			System.out.println("----------------------------");
			System.out.printf("%-12s%-5s", "-20","|");
			System.out.printf(" %10.2f\n", -28.8889);
			System.out.printf("%-12s%-5s", "-10","|");
			System.out.printf(" %10.2f\n", -23.3333);
			System.out.printf("%-12s%-5s", "+0","|");
			System.out.printf(" %10.2f\n", -17.7778);
			System.out.printf("%-12s%-5s", "+20","|");
			System.out.printf(" %10.2f\n", -6.6667);
			System.out.printf("%-12s%-5s", "+30","|");
			System.out.printf(" %10.2f\n", -1.1111);
			
			
	}
	
}
